# Lab5 -- Integration testing


You can find Decision Table and Test Cases table by the link below.
Google Spreadsheets were chosen because markdown tables are pain.

https://docs.google.com/spreadsheets/d/1qpAki2khToL060nz8w6QAgdRKZqos0lOc6c2afq4I3s/edit?usp=sharing

# Bugs
1. Price given on "nonsense"" as a `plan`.
2. Wrong prices are calculated for everything but `budget`, `minute`.
3. Discounts not included in calculations.
